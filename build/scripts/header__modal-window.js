const modalWindowLogin = document.getElementsByClassName('header__modal-container-login')[0];
const modalWindowRegister = document.getElementsByClassName('header__modal-container-register')[0];
const modalWindowEmptyBin = document.getElementsByClassName('header__modal-container-empty-bin')[0];
const modalWindowFullBin = document.getElementsByClassName('header__modal-container-full-bin')[0];
const login = document.getElementsByClassName('header__authorization-box-item-login')[0];
const register = document.getElementsByClassName('header__authorization-box-item-register')[0];
const bin = document.getElementsByClassName('fa-shopping-basket')[0];
const btnCloseLogin = document.getElementsByClassName('header__modal-close-login')[0];
const btnCloseRegister = document.getElementsByClassName('header__modal-close-register')[0];
const btnCloseEmptyBin = document.getElementsByClassName('header__modal-close-empty-bin')[0];
const btnCloseFullBin = document.getElementsByClassName('header__modal-close-full-bin')[0];
const amountProducts = document.getElementsByClassName('header__basket-amount-number')[0];

//open modal window for login
$(document).ready(function() {
    $(login).on('click', function() {
        $(modalWindowLogin).toggleClass("header__modal-container-login--active");
        $('html, body').css("overflow", "hidden");
    });
});

//open modal window for register
$(document).ready(function() {
    $(register).on('click', function() {
        $(modalWindowRegister).toggleClass("header__modal-container-register--active");
        $('html, body').css("overflow", "hidden");
    });
});

//open modal window for bin
$(document).ready(function() {
    $(bin).on('click', function() {
        if($(amountProducts).text() === '0') {
            $(modalWindowEmptyBin).toggleClass("header__modal-container-empty-bin--active");
        } else {
            $(modalWindowFullBin).toggleClass("header__modal-container-full-bin--active");
        }
        $('html, body').css("overflow", "hidden");
    });
});

//close modal window for login
$(document).ready(function() {
    $(btnCloseLogin).on('click', function() {
        $(modalWindowLogin).removeClass("header__modal-container-login--active");
        $('html, body').css("overflow", "auto");
    });
});

//close modal window for register
$(document).ready(function() {
    $(btnCloseRegister).on('click', function() {
        $(modalWindowRegister).removeClass("header__modal-container-register--active");
        $('html, body').css("overflow", "auto");
    });
});

//close modal window for empty bin
$(document).ready(function() {
    $(btnCloseEmptyBin).on('click', function() {
        $(modalWindowEmptyBin).removeClass("header__modal-container-empty-bin--active");
        $('html, body').css("overflow", "auto");
    });
});

//close modal window for full bin
$(document).ready(function() {
    $(btnCloseFullBin).on('click', function() {
        $(modalWindowFullBin).removeClass("header__modal-container-full-bin--active");
        $('html, body').css("overflow", "auto");
    });
});

