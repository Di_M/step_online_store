
// Show Furniture Filter modal
$(document).ready(function () {

    let modalOpen = false;

    $(window).resize(function () {
        if ($(window).width() >= 1200-17) {
            $('.furniture__filter-modal-background').css('display', 'block');
            $("html,body").css("overflow", "auto");
            $('body').css('padding-right', '0');
            modalOpen = false;
        }
        else {
            if(modalOpen === false) {
                $('.furniture__filter-modal-background').css('display', 'none');
                $("html,body").css("overflow", "auto");
                $('body').css('padding-right', '0');
            } else {
                $('.furniture__filter-modal-background').css('display', 'block');
            }
        }
    });

    $('.furniture__hamburger').click(function () {
        modalOpen = true;
        $("html,body").css("overflow", "hidden");
        $('body').css('padding-right', '17px');
        $('.furniture__filter-modal-background').fadeIn({
            start: function () {
                $(this).css({
                    display: "block"
                })
            }
        });
    });

    $('.furniture__filter-btn-close').click(function () {
        modalOpen = false;
        $('.furniture__filter-modal-background').fadeOut({
            start: function () {
                $(this).css({
                    display: "none"
                })
            }
        });
        $("html,body").css("overflow", "auto");
        $('body').css('padding-right', '0');
    });


});