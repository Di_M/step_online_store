// view or hide title for icons
const iconLike = document.getElementsByClassName("furniture-gallery-content-item-opportunities-block-icon-like");
const iconView = document.getElementsByClassName("furniture-gallery-content-item-opportunities-block-icon-view");
const iconBin = document.getElementsByClassName("furniture-gallery-content-item-opportunities-block-icon-bin");

const titleLike = document.getElementsByClassName("furniture-gallery-content-item-opportunities-block-title-like");
const titleView = document.getElementsByClassName("furniture-gallery-content-item-opportunities-block-title-view");
const titleBin = document.getElementsByClassName("furniture-gallery-content-item-opportunities-block-title-bin");

$(document).ready(function() {
    $(iconLike).on('click', function() {
        $(titleLike).toggleClass("furniture-gallery-content-item-opportunities-block-title-like--active");
    });
});
$(document).ready(function() {
    $(iconView).on('click', function() {
        $(titleView).toggleClass("furniture-gallery-content-item-opportunities-block-title-view--active");
    });
});
$(document).ready(function() {
    $(iconBin).on('click', function() {
        $(titleBin).toggleClass("furniture-gallery-content-item-opportunities-block-title-bin--active");
    });
});

// slider
let slideOfIndex = 1;
const showActiveSlide = (indexSlide) => {
    if (indexSlide > $('.furniture-gallery-content-slider-list li').length) {
        slideOfIndex = 1;
    } else if(indexSlide <= 0) {
        slideOfIndex = 4;
    }
    $('.furniture-gallery-content-slider-list li').removeClass('slider-item-active');
    let newActiveSlide = $('.furniture-gallery-content-slider-list li').eq(slideOfIndex - 1);
    newActiveSlide.addClass('slider-item-active');
    $(`[data-gallery-item = "${newActiveSlide.data('slider-list-item')}"]`)
        .addClass('furniture-gallery-content-item--active')
        .siblings('[data-gallery-item]')
        .removeClass('furniture-gallery-content-item--active');
};

const slideNext = (indexSlide) => {
    showActiveSlide(slideOfIndex += indexSlide);
};

const slidePrev = (indexSlide) => {
    showActiveSlide(slideOfIndex -= indexSlide);
};


$('.furniture-gallery-content-slider-btn--prev').click(function () {
    slidePrev(1);
});

$('.furniture-gallery-content-slider-btn--next').click(function () {
    slideNext(1);
});

$('[data-slider-list-item]').click(function () {
    slideOfIndex = $(this).data('slider-list-item');
    showActiveSlide(slideOfIndex);
});

//tabs
$('[data-works]').click(function () {
    $(this).addClass('active').siblings('[data-works]').removeClass('active');
    $(`[data-tab-item = "${$(this).data('works')}"]`)
        .addClass('active')
        .siblings('[data-tab-item]')
        .removeClass('active');
    $(`[data-image = "${$(this).data('works')}"]`)
        .addClass('active')
        .siblings('[data-image]')
        .removeClass('active');
});



//Change value in Header basket when click on button "Add ro Card"
$(document).ready(function () {
    $('.furniture-gallery-content-item-opportunities-block-icon-bin').click(function () {
        $('.header__basket-amount-number').text(`${+$('.header__basket-amount-number').text() + 1}`);
    });
});




