
//Show or Hide button more
function fadeButtonMore() {
    if($('.furniture__products-item-container:hidden').length === 0) {
        $('.furniture__products-btn-more').fadeOut('slow');
    } else {
        $('.furniture__products-btn-more').fadeIn({
            start: function () {
                $(this).css({
                    display: "flex"
                })
            }
        }, 'slow');
    }
}


//Show Products Item
function showProductsItem() {
    if ($(window).width() < 768-17) {
        $('.furniture__products-item-container:hidden').slice(0, 1).slideDown({
            start: function () {
                $(this).css({
                    display: "flex"
                })
            }
        });
        fadeButtonMore();
    } else if ($(window).width() < 1200-17) {
        $('.furniture__products-item-container:hidden').slice(0, 2).slideDown({
            start: function () {
                $(this).css({
                    display: "flex"
                })
            }
        });
        fadeButtonMore();
    } else {
        $('.furniture__products-item-container:hidden').slice(0, 3).slideDown({
            start: function () {
                $(this).css({
                    display: "flex"
                })
            }
        });
        fadeButtonMore();
    }
    $('h2.animate').css("display", "none");
    $("html,body").css("overflow", "auto");
}




//Show Product Item

$(document).ready(function () {

    if ($(window).width() < 768-17) {
        $('.furniture__products-item-container').slice(0, 3).css('display', 'flex');
        fadeButtonMore();
    } else if ($(window).width() < 1200-17) {
        $('.furniture__products-item-container').slice(0, 4).css('display', 'flex');
        fadeButtonMore();
    } else {
        $('.furniture__products-item-container').slice(0, 9).css('display', 'flex');
        fadeButtonMore();
    }


    $(window).resize(function () {
        if ($(window).width() < 768-17) {
            $('.furniture__products-item-container').css('display', 'none');
            $('.furniture__products-item-container').slice(0, 3).css('display', 'flex');
            fadeButtonMore();
        } else if ($(window).width() < 1200-17) {
            $('.furniture__products-item-container').css('display', 'none');
            $('.furniture__products-item-container').slice(0, 4).css('display', 'flex');
            fadeButtonMore();
        } else {
            $('.furniture__products-item-container').css('display', 'none');
            $('.furniture__products-item-container').slice(0, 9).css('display', 'flex');
            fadeButtonMore();
        }
    });
});



// Furniture product Button "Load More"
$(document).ready(function () {

    $('.furniture__products-btn-more').click(function () {
        $('h2.animate').css('display', 'flex');
        $("html,body").css("overflow", "hidden");
        setTimeout(function () {
            showProductsItem();
        }, 3000);
    });

});



//Add active class to amount block in Furniture filter by category
$(document).ready(function () {
    $('.furniture__filter-category-list-item').click(function () {
        $(this).children('.furniture__filter-list-item-amount').toggleClass('furniture__filter-list-item-amount-active');
        $(this).toggleClass('furniture__filter-list-item-active');
    });
});



//Add active class to amount block in Furniture filter by size
$(document).ready(function () {
    $('.furniture__filter-size-checkbox').change(function () {
        $(this).parent().next().toggleClass('furniture__filter-list-item-amount-active');
    });
});



//Add active class to tags button in Furniture filter by tags
$(document).ready(function () {
    $('.furniture__filter-tags-item').click(function () {
        $(this).toggleClass('furniture__filter-tags-item--active');
    });
});


//Change value in Header basket when click on button "Add ro Card"
$(document).ready(function () {
    $('.furniture__products-item-btn-add-to-card').click(function () {
        $('.header__basket-amount-number').text(`${+$('.header__basket-amount-number').text() + 1}`);
    });
    $('.quick-view__item-btn-add-to-card').click(function () {
        $('.header__basket-amount-number').text(`${+$('.header__basket-amount-number').text() + 1}`);
    });
});
