const navigation = document.getElementsByClassName('header__navigation-menu')[0];
const hamburgerDiv = document.getElementsByClassName('header__hamburger')[0];
const hamburger = document.getElementsByClassName('fa-bars')[0];
const iconHamburger = document.getElementsByClassName('header__hamburger-icon')[0];
const menuItem = document.getElementsByClassName('.header__menu-item');

$(document).ready(function() {
    $(hamburger).on('click', function() {
        $(navigation).toggleClass("header__navigation-menu--active");
        $(iconHamburger).toggleClass("fa-times");
        $(iconHamburger).toggleClass("header__hamburger-icon--close");
        $(hamburgerDiv).toggleClass("header__hamburger--close");
    });
});
$(document).ready(function () {
    $(menuItem).on('click', function () {
        $(menuItem).toggleClass('.header__menu-item--active');
    })
});

$('a[href*=\\#]').on('click', function () {
    let selectedNavItem = $(this).attr('href');
    $('html, body').stop().animate({
        scrollTop: $(selectedNavItem).offset().top
    },2000);
    $(navigation).removeClass('header__navigation-menu--active');
    $(hamburgerDiv).removeClass('header__hamburger--close');
    $(iconHamburger).toggleClass("fa-times");
    $(iconHamburger).removeClass("header__hamburger-icon--close");
});


$(window).on('scroll', function () {
    if($(this).scrollTop() > $(this).height()) {
        $('.container__btn-page-up').show();
    } else {
        $('.container__btn-page-up').hide();
    }
});


$('.container__btn-page-up').on('click', function () {
    $('html, body').stop().animate({
        scrollTop: 0
    },3000);
});