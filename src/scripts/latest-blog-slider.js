//Helpers
let slideIndex = 0;



//Functions for content-items
function showContentItem() {
    slideIndex = 0;
    $('.latest-blog__dots-item').removeClass('latest-blog__dots-item--active');
    $('.latest-blog__dots-item').eq(0).addClass('latest-blog__dots-item--active');
    if ($(window).width() < 768-17) {
        $('.latest-blog__content-item').each(function (index, value) {
            if (index === slideIndex) {
                $(this).css('display', 'flex');
            } else {
                $(this).css('display', 'none');
            }
        });
    } else if ($(window).width() < 1200-17) {
        if (slideIndex === $('.latest-blog__content-item').length - 1) {
            $('.latest-blog__content-item').each( function (index, value) {
                if (index === slideIndex || index === 0) {
                    $(this).css('display', 'flex');
                } else {
                    $(this).css('display', 'none');
                }
            });
        } else {
            $('.latest-blog__content-item').each( function (index, value) {
                if (index === slideIndex || index === slideIndex + 1) {
                    $(this).css('display', 'flex');
                } else {
                    $(this).css('display', 'none');
                }
            });
        }
    } else {
        if (slideIndex === $('.latest-blog__content-item').length - 1) {
            $('.latest-blog__content-item').each( function (index, value) {
                if (index === slideIndex || index === 0 || index === 1) {
                    $(this).css('display', 'flex');
                } else {
                    $(this).css('display', 'none');
                }
            });
        } else {
            $('.latest-blog__content-item').each( function (index, value) {
                if (index === slideIndex || index === slideIndex + 1 || index === slideIndex + 2) {
                    $(this).css('display', 'flex');
                } else {
                    $(this).css('display', 'none');
                }
            });
        }
    }
}



//Functions for Slider with buttons
function showSlide(n) {
    if (n > $('.latest-blog__content-item').length - 1) slideIndex = 0;
    if (n < 0) slideIndex = $('.latest-blog__content-item').length - 1;
    $('.latest-blog__content-item').each(function () {
        $(this).css('display', 'none');
    });
    $('.latest-blog__content-item').eq(slideIndex).css('display', 'flex');
}

function nextSlide(n) {
    showSlide(slideIndex += n);
}

function prevSlide(n) {
    showSlide(slideIndex -= n);
}



// Events
$(document).ready(function() {

    showContentItem();

    $(window).resize(function () {
        showContentItem();
    });

    $('.latest-blog__btn-next').click( function () {
        nextSlide(1);
    });

    $('.latest-blog__btn-prev').click( function () {
        prevSlide(1);
    });

    $('.latest-blog__dots-item').click(function () {
        $(this).addClass('latest-blog__dots-item--active')
            .siblings().removeClass('latest-blog__dots-item--active');
        if ($(window).width() < 1200) {
            $('[data-md-item]').css('display', 'none')
                .siblings(`[data-md-item = "${$(this).data('mdDots')}"]`).css('display', 'flex');
        } else {
            $('[data-lg-item]').css('display', 'none')
                .siblings(`[data-lg-item = "${$(this).data('lgDots')}"]`).css('display', 'flex');
        }

    });
});
