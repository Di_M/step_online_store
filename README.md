Step project - "Online store"<br>
created by Bohdan Budak and Murashko Dmytro<br>

IMPORTANT!!!<br>
First of all type comand - npm install<br>
Build project - npm run build<br>
Build project and open in browser - npm run start<br>
Serve in browser - npm run serve<br>
Clean build folder - npm run clean<br>


Tasks Student_1 - Bohdan Budak:<br>
1.Created Header including Navbar, Top-menu and modals Basket, Login and Register.<br>
2.Created Top-slider.<br>
3.Created Features.<br>
4.Created Furniture Gallery.<br>


Tasks Student_2 - Murashko Dmytro:<br>
1.Created Stocks.<br>
2.Created Furniture including modal QuickView and Add to Basket.<br>
3.Created Filters.<br>
4.Created Blog.<br>
5.Created Subscribe.<br>
6.Created Footer including Copyright.<br>
