const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const clean = require('gulp-clean');
const plumber = require('gulp-plumber');
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');



// Clean folder "build"
function cleanBuild() {
    return gulp.src('./build', { read: false, allowEmpty: true })
        .pipe(clean())
}



// Copy HTML to folder "build"
function copyHTML() {
    return gulp.src('./src/index.html')
        // .pipe(plumber())
        .pipe(gulp.dest('./build'))
        .pipe(browserSync.reload({stream: true}))
}



// Clean and copy all fonts to folder "build"
// function cleanFonts() {
//     return gulp.src('./build/fonts', { read: false, allowEmpty: true })
//         .pipe(clean())
// }

// function copyFonts() {
//     return gulp.src('./src/fonts/**/*')
//         .pipe(gulp.dest('./build/fonts'))
// }



// Clean and copy all images to folder "build"
function cleanImages() {
    return gulp.src('./build/images', { read: false, allowEmpty: true })
        .pipe(clean())
}

function copyImages() {
    return gulp.src('./src/images/**/*')
        // .pipe(imagemin({optimizationLevel: 5}))
        .pipe(gulp.dest('./build/images'))
}



// Add jquery, bootstrap, popper and owner script to folder "build"
function copyJsFiles() {
    return gulp.src([
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/bootstrap/dist/js/bootstrap.min.js',
        './node_modules/popper.js/dist/umd/popper.min.js',
        './src/scripts/**/*'
    ])
        // .pipe(plumber())
        .pipe(gulp.dest('./build/scripts'))
        .pipe(browserSync.reload({stream: true}))
}



// Transform style.scss to style.css and push to folder "build"
function sassToCss() {
    return gulp.src([
        './src/scss/style.scss',
        './src/scss/style-768px.scss',
        './src/scss/style-1200px.scss',
        './src/scss/reset.scss'
    ])
        // .pipe(plumber())
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gulp.dest('./build/style'))
        .pipe(browserSync.reload({stream: true}))
}



// Watcher for html, scss, images, fonts and scripts
function watch() {
    gulp.watch('./src/index.html', copyHTML);
    gulp.watch('./src/scss/*.scss', sassToCss);
    // gulp.watch('./src/fonts/**/*', gulp.series(cleanFonts, copyFonts));
    gulp.watch('./src/images/**/*', gulp.series(cleanImages, copyImages));
    gulp.watch('./src/scripts/**/*', copyJsFiles);
}



// Reload work pages and watch for updates in changes files
function serve() {
    browserSync.init({
        server: './build'
    });
    watch();
}



// Exports command for using in package.json scripts
exports.clean = cleanBuild;
exports.build = gulp.series(cleanBuild, copyHTML, /*copyFonts,*/ copyImages, copyJsFiles, sassToCss);
exports.serve = serve;